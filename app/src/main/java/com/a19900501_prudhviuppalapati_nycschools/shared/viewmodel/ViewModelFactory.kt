package com.a19900501_prudhviuppalapati_nycschools.shared.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.a19900501_prudhviuppalapati_nycschools.data.api.ApiHelper
import com.a19900501_prudhviuppalapati_nycschools.data.respository.SchoolRepositoryImpl
import com.a19900501_prudhviuppalapati_nycschools.ui.schoollist.SchoolDirectoryViewModel
import com.a19900501_prudhviuppalapati_nycschools.ui.satscore.SatScoreViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(SchoolDirectoryViewModel::class.java) -> {
                SchoolDirectoryViewModel(SchoolRepositoryImpl(apiHelper)) as T
            }
            modelClass.isAssignableFrom(SatScoreViewModel::class.java) -> {
                SatScoreViewModel(SchoolRepositoryImpl(apiHelper)) as T
            }
            else -> throw IllegalArgumentException("Unknown class name")
        }
    }

}