package com.a19900501_prudhviuppalapati_nycschools.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SchoolList(val dbn:String,
                      val school_name:String,
                      val boro:String,
                      val overview_paragraph:String,
                      val total_students:String,
                      val location:String) : Parcelable
