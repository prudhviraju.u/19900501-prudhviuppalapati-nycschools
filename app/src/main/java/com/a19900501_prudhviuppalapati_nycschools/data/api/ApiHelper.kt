package com.a19900501_prudhviuppalapati_nycschools.data.api

import com.a19900501_prudhviuppalapati_nycschools.data.model.SatScore
import com.a19900501_prudhviuppalapati_nycschools.data.model.SchoolList


interface ApiHelper {

    suspend fun getSchoolList(): List<SchoolList>
    suspend fun getSatScoreList():List<SatScore>
}