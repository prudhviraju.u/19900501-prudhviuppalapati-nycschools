package com.a19900501_prudhviuppalapati_nycschools.data.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}