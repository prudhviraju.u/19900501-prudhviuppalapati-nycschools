package com.a19900501_prudhviuppalapati_nycschools.data.respository

import com.a19900501_prudhviuppalapati_nycschools.data.api.ApiHelper

class SchoolRepositoryImpl(private val apiHelper: ApiHelper):SchoolRepository {
    override suspend fun getSchoolList() =  apiHelper.getSchoolList()
    override suspend fun getSatScore() = apiHelper.getSatScoreList()
}