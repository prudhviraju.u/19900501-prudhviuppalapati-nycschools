package com.a19900501_prudhviuppalapati_nycschools.data.respository

import com.a19900501_prudhviuppalapati_nycschools.data.model.SatScore
import com.a19900501_prudhviuppalapati_nycschools.data.model.SchoolList

interface SchoolRepository {
    suspend fun getSchoolList() : List<SchoolList>
    suspend fun getSatScore():List<SatScore>
}