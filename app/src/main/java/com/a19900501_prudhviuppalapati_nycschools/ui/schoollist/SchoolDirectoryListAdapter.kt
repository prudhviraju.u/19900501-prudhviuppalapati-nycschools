package com.a19900501_prudhviuppalapati_nycschools.ui.schoollist

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.a19900501_prudhviuppalapati_nycschools.R
import com.a19900501_prudhviuppalapati_nycschools.data.model.SchoolList
import com.a19900501_prudhviuppalapati_nycschools.ui.satscore.SatScoreActivity

class SchoolDirectoryListAdapter(private val dataSet: ArrayList<SchoolList>,
                                 private val context:Activity) :
    RecyclerView.Adapter<SchoolDirectoryListAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val schoolName: TextView
        val studentCount : TextView
        val studentLocation : TextView

        init {
            // Define  listener for the ViewHolder's View.
            schoolName = view.findViewById(R.id.school_name)
            studentCount = view.findViewById(R.id.student_count)
            studentLocation = view.findViewById(R.id.student_address)
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_school_list, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.schoolName.text = dataSet[position].school_name
        viewHolder.studentLocation.text = dataSet[position].location
        viewHolder.studentCount.text = "${dataSet[position].total_students} Students"
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(context,SatScoreActivity::class.java)
            intent.putExtra("SCHOOL",dataSet[position])
            context.startActivity(intent)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}