package com.a19900501_prudhviuppalapati_nycschools.ui.schoollist

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.a19900501_prudhviuppalapati_nycschools.data.api.ApiHelperImpl
import com.a19900501_prudhviuppalapati_nycschools.data.api.RetrofitBuilder
import com.a19900501_prudhviuppalapati_nycschools.data.api.Status
import com.a19900501_prudhviuppalapati_nycschools.data.model.SchoolList
import com.a19900501_prudhviuppalapati_nycschools.databinding.ActivityMainBinding
import com.a19900501_prudhviuppalapati_nycschools.shared.base.BaseActivity
import com.a19900501_prudhviuppalapati_nycschools.shared.viewmodel.ViewModelFactory

class SchoolDirectoryListActivity : BaseActivity() {

    private lateinit var mbinding: ActivityMainBinding
    private lateinit var schoolDirectoryViewModel: SchoolDirectoryViewModel
    private lateinit var adapter : SchoolDirectoryListAdapter
    private val schoolItemList = ArrayList<SchoolList>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mbinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mbinding.root)

        setupViewModel()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupViewModel() {
        schoolDirectoryViewModel = ViewModelProvider(
            this,
            ViewModelFactory(ApiHelperImpl(RetrofitBuilder.apiService))
        ).get(SchoolDirectoryViewModel::class.java)
    }

    private fun setupRecyclerView() {
        adapter =  SchoolDirectoryListAdapter(schoolItemList,this)
        mbinding.schoolList.layoutManager = LinearLayoutManager(applicationContext)
        mbinding.schoolList.adapter = adapter
    }

    private fun setupObservers() {
        schoolDirectoryViewModel.getSchoolDirectoryList().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { schoolList: List<SchoolList> ->
                        renderSchoolList(schoolList)
                    }
                }
                Status.LOADING -> {
                    showProgressBar()
                }
                else -> {
                    hideProgressBar()
                }
            }
        }
    }

    private fun renderSchoolList(schoolList: List<SchoolList>) {
        schoolItemList.addAll(schoolList)
        adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        if(schoolDirectoryViewModel!=null){
            schoolItemList.clear()
            schoolDirectoryViewModel.fetchSchoolDirectoryList()
        }
    }
}