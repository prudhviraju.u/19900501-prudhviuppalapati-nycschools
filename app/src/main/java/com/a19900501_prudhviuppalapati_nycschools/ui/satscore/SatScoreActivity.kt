package com.a19900501_prudhviuppalapati_nycschools.ui.satscore

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.a19900501_prudhviuppalapati_nycschools.R
import com.a19900501_prudhviuppalapati_nycschools.data.api.ApiHelperImpl
import com.a19900501_prudhviuppalapati_nycschools.data.api.RetrofitBuilder
import com.a19900501_prudhviuppalapati_nycschools.data.api.Status
import com.a19900501_prudhviuppalapati_nycschools.data.model.SatScore
import com.a19900501_prudhviuppalapati_nycschools.data.model.SchoolList
import com.a19900501_prudhviuppalapati_nycschools.databinding.ActivitySatscoreBinding
import com.a19900501_prudhviuppalapati_nycschools.shared.base.BaseActivity
import com.a19900501_prudhviuppalapati_nycschools.shared.viewmodel.ViewModelFactory
import com.a19900501_prudhviuppalapati_nycschools.utils.showToast

class SatScoreActivity : BaseActivity() {

    private lateinit var mbinding: ActivitySatscoreBinding
    private lateinit var satScoreViewModel: SatScoreViewModel
    private val schoolSatScoreList = ArrayList<SatScore>()
    private lateinit var selectSchool : SchoolList
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mbinding = ActivitySatscoreBinding.inflate(layoutInflater)
        setContentView(mbinding.root)

        selectSchool = intent.getParcelableExtra<SchoolList>("SCHOOL") as SchoolList
        selectSchool.let {
            setupViewModel()
            setupObservers()
        }
    }

    private fun setupViewModel() {
        satScoreViewModel = ViewModelProvider(
            this,
            ViewModelFactory(ApiHelperImpl(RetrofitBuilder.apiService))
        ).get(SatScoreViewModel::class.java)
    }

    private fun setupObservers() {
        satScoreViewModel.getSchoolSatScoreList().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { schoolSatScoreList: List<SatScore> ->
                        renderSatScoreList(schoolSatScoreList)
                    }
                }
                Status.LOADING -> {
                    showProgressBar()
                }
                else -> {
                    hideProgressBar()
                }
            }
        }
    }

    private fun renderSatScoreList(satScoreList: List<SatScore>) {
        if(satScoreList.isNotEmpty()) {
            val selectDbnScore = satScoreList.firstOrNull { it.dbn == selectSchool.dbn }
            if(selectDbnScore == null){
                showToast(this, getString(R.string.sat_results_not_available) )
                finish()
            }
            selectDbnScore?.let {
                mbinding.math.text = "Math :${it.sat_math_avg_score}"
                mbinding.reading.text = "Critical Reading:${it.sat_critical_reading_avg_score}"
                mbinding.writing.text = "Writing :${it.sat_writing_avg_score}"
                mbinding.addressLine.text = selectSchool.location
                mbinding.studentsCount.text = "${selectSchool.total_students} Students"
                mbinding.satTestTakers.text = it.num_of_sat_test_takers
            }
        }
    }
}